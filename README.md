Parallel Ranking and Selection on Spark
===============

This is an Apache Spark implementation of the GSP procedure for solving Ranking and Selection problems. The procedure is described in detail in the working paper 

> Ni, Eric C., Dragos F. Ciocan, Susan R. Hunter, Shane G. Henderson (2015), "Efficient Ranking and Selection in Parallel Computing Environments". http://arxiv.org/abs/1506.04986

To compile, simply run the following command (which uses [SBT](http://www.scala-sbt.org/)) and a jar file will appear in the */target* folder. 

    sbt/sbt clean package

The default version is built against Spark 1.2.0.
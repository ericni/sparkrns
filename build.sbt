name := "sparkrns"

version := "0.1"

scalaVersion := "2.10.4"

// additional libraries
libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.0" % "provided"

#!/bin/bash

export HADOOP_CONF_DIR=/etc/hadoop/conf
export NCORES=480
export NGROUPS=423
IDCOUNTER=0

for RB    in 128      ; do
for DELTA in 0.1      ; do
for BSIZE in 100      ; do
for S1MAX in 1000     ; do
for COV   in -1       ; do
for N1    in 50       ; do
IDCOUNTER=$[$IDCOUNTER + 1]
spark-submit --master yarn-cluster \
	--class driver.SparkRnS \
        --num-executors 141 --executor-cores 3 --executor-memory 7G \
	target/scala-2.10/sparkrns_2.10-0.1.jar \
	out $N1 $BSIZE $NCORES $NGROUPS `expr $IDCOUNTER + 14850` \
	$S1MAX $DELTA $RB $COV
done
done
done
done
done
done

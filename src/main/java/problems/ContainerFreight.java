package problems;
import util.*;
import java.util.*;

/**
 *
 * Implements Container Freight test problem.
 *
 * @author Eric Ni, cn254@cornell.edu
 */
public class ContainerFreight extends SOProb {

	/**
	 * Parameter M, the number of available docks. Determines size of the problem. 
	 */
	int _param_M = 115;

	/**
	 * Variance of the Log-normal distribution used to randomize burn-in.
	 * Set to -1 to fix burn-in to the default value of 100.
	 */
	double burnin_param = -1;

	/**
	 * 
	 * @param s Converted to RB.
	 * @param cov Converted to burnin_param.
	 */
	public ContainerFreight(String s, String cov) {
		_param_M = Integer.parseInt(s);
		burnin_param = Double.parseDouble(cov);
		double []ls = {52.8, 11.7, 13.0, 22.5};
		double []us = {67.0, 46.0, 92.0, 34.0};
		lambdas = new double[nTypes];
		mus = new double[nTypes];
		LB = new int [nTypes]; 
		for (int i = 0; i < nTypes; ++i) {
			lambdas[i] = ls[i]/60.;
			mus[i] = 1./us[i];
			LB[i] = (int) Math.ceil( lambdas[i] / mus[i] );
		}
	}

	/**
	 * Calculates the number of systems under the given parameter.
	 * 
	 * @param Parameter M.
	 * @return The number of systems with M=param.
	 * @throws Exception 
	 */
	public static long getNumSystems(String param) throws Exception {
		long _M = Long.parseLong(param);
		return Combination.Choose(_M - 101 + 3, 3);
	}

	/**
	 * Number of types of job. 
	 */
	int nTypes = 4;

	/**
	 * Decision variables.
	 */
	int[] x_disc;

	/**
	 * Lower bound on decision variables. To be computed in the constructor.
	 */
	int [] LB ;

	/**
	 * Arrival rates
	 */
	double [] lambdas ;	

	/**
	 * Service rates
	 */
	double [] mus ;

	/**
	 * Converts a system ID to the array of decision variables and store in
	 * {@link #x_disc} 
	 */
	private void Idx2System_Disc() {
		int nTypes = 4;
		x_disc = new int [nTypes];

		int base = 0;
		int i;
		for ( i = 0; i < nTypes; ++i ) base += LB[i];
		long[] ans = null;
		try {
			ans = new Combination(_param_M - base + nTypes - 1,
					nTypes - 1 ).Element(sysid).getData();
//			System.out.println( "Ans length = " + ans.length );
		} catch (Exception e) {
			e.printStackTrace();
		}
		for ( i = 0; i < nTypes - 1; ++i ) ans[i] += 1;

		int []x = new int[nTypes];
		x[0] = (int) (ans[0] - 1 + LB[0]);
		x[nTypes - 1] = _param_M - x[0];
		for (i = 1; i < nTypes - 1; ++i ) {
			x[i] = (int) (ans[i] - ans[i-1] - 1 + LB[i]);
			x[nTypes - 1] -= x[i];
		}
		x_disc = x;
	}

	/**
	 * Simulate the container freight problem.
	 */
	public void runSystem() {
		this.Idx2System_Disc();
		long t0 = System.nanoTime();

		int [] x = x_disc;
		int i,j; 
		int _numReps = numReplications;

		//Matrix<double> ServiceT  (param->nstages, param->njobs+1, 0.0);
		int njobs = 600 * 60;
		int burnin= 100 * 60;	//number of burn-in jobs
		double Tmax;

		int[] burnins = null;
		if(burnin_param > 0) {

			//lognormal
			double mu = Math.log(burnin) - burnin_param / 2.;
			double sig = Math.sqrt(burnin_param);			
			burnins = new int[_numReps];
			for (i = 0; i < _numReps; ++i)  {
				int v = (int) Math.exp( rStream.RandNormal(0., 1.) * sig + mu );
				burnins[i] = (v > 20000) ? 20000:v;
			}
			/*
			//weibull shape = 0.3, scal = 10
			burnins = new int[numReplications];
			for (i = 0; i < numReplications; ++i) {
				 int v = (int) (Math.pow((- Math.log( 1. - rStream.randU01() )), 0.1) * 0.3);
				 burnins[i] = (v > 10000) ? 10000:v;
			}
			 */
		}
		int _burnin;
		int _diff = njobs - burnin;

		double[] avg_wait_time = new double[_numReps];
		double total_wait_time = 0.0;
		this.ans.FnSumSq = 0.0;
		int[][] Poisson = new int [_numReps][nTypes];

		for ( int k = 0; k < _numReps; ++k ) {
			_burnin = (burnin_param > 0) ? burnins[k] : burnin;
			Tmax = _burnin + _diff;
			for ( j = 0; j < nTypes; ++j ) {
				Poisson[k][j]= rStream.
						RandPoisson(lambdas[j] * Tmax);
			}
		}

		double[][] Arrv = new double[nTypes] [];
		double[][] Svcs = new double[nTypes] [];
		double[][] Exit = new double[nTypes] [];

		for (int k=0; k<numReplications; ++k) {
			_burnin = (burnin_param > 0) ? burnins[k] : burnin;
			Tmax = _burnin + _diff;

			for (j = 0; j < nTypes; ++j ) {
				int sz = Poisson[k][j];
				//			maxSize = maxSize < sz ? sz : maxSize;
				Arrv[j] = new double[sz];
				Svcs[j] = new double[sz];
				Exit[j] = new double[sz];
				for ( i = 0; i < sz; ++i ) {
					Arrv[j][i] = rStream.randU01() * Tmax;
					Svcs[j][i] = rStream.RandExponential(mus[j]);
				}
				Arrays.sort(Arrv[j]);

				////////////////////////////////////
				// done generating random numbers
				///////////////////////////////////

				///////////////////////
				// begin simulation
				///////////////////////

				/**********
				 * New simulation: using priority queue to simulate a MMc queue
				 */
				Comparator<MMCEvent> comparator = new MMCEventComparator();
				PriorityQueue<MMCEvent> event_q = 
						new PriorityQueue<MMCEvent>(sz, comparator);
				Queue< MMCEvent> wait_q = new LinkedList<MMCEvent>();
				int numJobsInService = 0;
				double currentTime = 0;
				MMCEvent currentEvent;
				for (i = 0; i < sz; ++i) {
					event_q.add( new MMCEvent( i, 
							Arrv[j][i], Arrv[j][i], -1, MMCEventType.arrival_event ) );
				}
				while(!event_q.isEmpty()) {
					currentEvent = event_q.remove();
					currentTime = currentEvent.event_time;
					if(currentEvent.type == MMCEventType.arrival_event) {
						if (numJobsInService < x[j]) {
							++numJobsInService;
							assert(currentEvent.departure_time < 0);
							currentEvent.type = MMCEventType.departure_event;
							Exit[j][currentEvent.id]
									= currentEvent.event_time
									= currentEvent.departure_time
									= currentTime + Svcs[j][currentEvent.id];
							event_q.add(currentEvent);
						} else
							wait_q.add(currentEvent);
					} else if (currentEvent.type == MMCEventType.departure_event) {
						if(!wait_q.isEmpty()) {
							assert( numJobsInService == x[j] );
							currentEvent = wait_q.remove();
							assert(currentEvent.departure_time < 0);
							currentEvent.type = MMCEventType.departure_event;
							Exit[j][currentEvent.id]
									= currentEvent.event_time
									= currentEvent.departure_time
									= currentTime +	Svcs[j][currentEvent.id];
							event_q.add(currentEvent);
						} else
							--numJobsInService;
					} else {
						//corrupted
					}
				}
			}

			double totalTime = 0.0;
			int totalJobs = 0;
			for ( j = 0; j < nTypes; ++j ) {
				int sz = Poisson[k][j];
				double timePerType = 0.0;
				int jobPerType = 0;
				for (i=0; i<sz; ++i) {
					if (Arrv[j][i] >= _burnin) {
						timePerType = timePerType + Exit[j][i] - Arrv[j][i];
						jobPerType += 1;
					}
				}
				totalTime += timePerType;
				totalJobs += jobPerType;
			}
			// changing to negative value since algorithms are maximizing
			avg_wait_time[k] = - totalTime / totalJobs;
			total_wait_time += avg_wait_time[k];
			this.ans.FnSumSq += avg_wait_time[k]*avg_wait_time[k];
		}
		//final answer
		this.ans.simtime = Math.max(2, System.nanoTime()-t0);
		this.ans.fn = total_wait_time/numReplications;
		this.ans.FnVar = 0.0;
		for (int k=0; k<numReplications; k++)  this.ans.FnVar += Math.pow(avg_wait_time[k]-this.ans.fn,2);
		this.ans.FnVar = this.ans.FnVar / (numReplications-1);
	}
}


package problems;

/**
 * A simple class representing events in M/M/c queue simulation
 *
 */
public class MMCEvent {
	
	public MMCEvent( int _id,
			double _event_t,
			double _arr_t,
			double _dep_t,
			MMCEventType _type ) {
		
			id=_id;
			arrival_time=_arr_t;
			departure_time=_dep_t;
			type=_type;
			event_time =_event_t;
	}
	/**
	 * Index of the job, used to locate service time.
	 */
	public int id; 
	/**
	 * Type of event, either arrival_event or departure_event.
	 */
	public MMCEventType type = MMCEventType.arrival_event;
	/**
	 * Time at which the event occurs (in minutes)
	 */
	public double event_time; 
	/**
	 * Arrival time (in minutes)
	 */
	public double arrival_time; 
	/**
	 * Departure time (in minutes)
	 */
	public double departure_time = -1; 
}
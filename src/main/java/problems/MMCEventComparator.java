package problems;

import java.util.Comparator;

/**
 * A comparator class that sorts MMCEvent objects by event time in ascending order
 *
 */
public class MMCEventComparator implements Comparator<MMCEvent>
{
    @Override
    public int compare(MMCEvent x, MMCEvent y)
    {
        // Assume neither string is null. Real code should
        // probably be more robust
        // You could also just return x.length() - y.length(),
        // which would be more efficient.
        if (x.event_time < y.event_time)
        {
            return -1;
        }
        if (x.event_time > y.event_time)
        {
            return 1;
        }
        return 0;
    }
}

package problems;

/**
 * Simple enumerator that defines types of MMC Events
 *
 */
public enum MMCEventType {
	arrival_event, departure_event, null_event 
}

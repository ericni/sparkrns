/**
 * The L'Ecuyer (2001) Random number generator with additional functions.
 * 
 * http://www.iro.umontreal.ca/~lecuyer/myftp/streams00/java/
 *
*/
package rngstream;
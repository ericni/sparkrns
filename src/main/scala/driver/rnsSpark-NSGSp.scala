/**
 */
package driver;

import org.apache.spark._
import org.apache.spark.SparkContext._
import rngstream._;
import problems._;
import org.apache.commons.math3.distribution._

object SparkRnS_NSGSp {
	@SerialVersionUID(124L)
	class sysEntry(id_in: Int, fn_in: Double, n_in: Int,
			bsize_in: Int, S2_in: Double, stream_in: String, rmax_i_in: Int = -1) extends Serializable
			{
		var id: Int = id_in;
	var fn: Double = fn_in;
	var n: Int = n_in;
	val bsize: Int = bsize_in;
	val S2: Double = S2_in;
	var stream: String = stream_in;
	var rmax_i: Int = rmax_i_in;

	override def toString() : String = {
			val collected = (",",id, fn, n, bsize, S2, stream, rmax_i,",");
			collected.toString();
	}

	def eliminates(j: sysEntry, n1: Int, t: Double, delta: Double) : Boolean = {
			val Xbar_j = j.fn;
			val S2j = j.S2;
			val Xbar_i = this.fn;
			val S2i = this.S2;
			val rhs = math.max(t * Math.sqrt((S2i + S2j ) / n1) - delta, 0.0);
			val lhs = Xbar_i-Xbar_j;
			(lhs > rhs)
	}
			}

	def stage1mapper(id: Int, n1: Int, globalSeed: Long,
			param: String, cov: String,
			acc_sim: Accumulator[Long], acc_t: Accumulator[Long])
	: (Int, Double, Double, Long, String) = {
		val stream = new RngStream()
		val t = id.toLong + globalSeed
		val seed = Array(t,t,t,t,t,t)
		stream.setSeed(seed)
		val prob = new TpMax(param, cov)
		val start_time = System.nanoTime()
		prob.runSystem( id, n1, stream )
		val run_time = System.nanoTime() - start_time
		acc_t += run_time
		acc_sim += n1.toLong
		val ans = prob.getAns()
		val fn = ans.getFn()
		val s2 = ans.getFnVar()
		val seed_after = RngStream.SeedToStr(stream.getState().map( _.toLong ))
		(id, fn, s2, run_time, seed_after )
	}

	def batchsizemapper( entry: (Int, Double, Double, Long, String),
			n1: Int, nGroups: Int, batchSize: Int)
	: ( Int, sysEntry ) = {
		val groupID = SOProb.get_group(entry._1, nGroups);
		val S2 = entry._3;
		val bsize = batchSize;
		val e = new sysEntry(entry._1, entry._2, n1, bsize, entry._3, entry._5);
		( groupID, e )
	}

	def runSimBatch( e: sysEntry, nGroups: Int, param: String, cov: String,
			acc_sim: Accumulator[Long], acc_t: Accumulator[Long], r_current: Int )
	: (Int, sysEntry ) = {
		val new_e = if (e.rmax_i > 0 && r_current > e.rmax_i) {
			e
		} else {
			val seed = RngStream.StrToSeed(e.stream);
			RngStream.setPackageSeed(seed);
			val stream = new RngStream();
			val prob = new TpMax(param, cov);
			val start_time = System.nanoTime();
			prob.runSystem( e.id, e.bsize, stream );
			val run_time = System.nanoTime() - start_time;
			acc_sim += e.bsize.toLong;
			acc_t += run_time;
			val fn = prob.getAns().getFn();
			val seed_after = RngStream.SeedToStr(stream.getState().map( _.toLong ));
			val new_n = e.n + e.bsize;
			val new_fn = (fn * e.bsize + e.fn * e.n) / new_n;
			new sysEntry( e.id, new_fn, new_n, e.bsize, e.S2, seed_after, e.rmax_i );
		}
		val groupID = SOProb.get_group( e.id, nGroups );
		( groupID, new_e )
	}

	def calcS2Sample( e: sysEntry, delta: Double,
			batchSize: Int, rinott_h: Double )
	: List[ (Int, (String, Int)) ] = {
		var out: List [ (Int, (String, Int)) ] = List();
	val RinottSize = Math.ceil(rinott_h * rinott_h * e.S2 / delta / delta).toInt;
	var additionalSize = RinottSize - e.n;
	additionalSize = if (additionalSize > 0) additionalSize else 0;
	var seed = RngStream.StrToSeed(e.stream);
	var rStream = new RngStream();
	rStream.setSeed(seed);
	while(additionalSize > 0) {
		val state = rStream.getState();
		seed = state.map( _.toLong );
		val seedStr = RngStream.SeedToStr(seed);
		val nextBatchSize = if (additionalSize > batchSize) batchSize else additionalSize;
		additionalSize -= nextBatchSize;
		out = out :+ ( e.id, (seedStr, nextBatchSize) );
		rStream.resetNextSubstream();
	}
	out
	}

	def runRinottSim( e: (Int, (String, Int)), param: String, cov: String,
			acc_sim: Accumulator[Long], acc_t: Accumulator[Long], run: Boolean )
	: (Int, (Double, Int) ) = {
		val sysid = e._1;
		val seed = RngStream.StrToSeed(e._2._1);
		val size = e._2._2;
		assert(size > 0);

		val rStream = new RngStream();
		rStream.setSeed(seed);
		val prob = new TpMax(param, cov);
		var _t = System.nanoTime();
		if (run) {
			prob.runSystem(sysid, size, rStream);
		}
		_t = System.nanoTime() - _t;
		acc_sim += size.toLong;
		acc_t += _t;
		val ans = prob.getAns();
		(sysid, (ans.getFn(), size));
	}

	def existingSample( e: sysEntry ) : (Int, (Double, Int) ) = {
		( e.id, (e.fn, e.n) )
	}

	def toList[A](a: A) = List(a);

	def ListVsEntry( sysList: List[sysEntry], nextSys: sysEntry,
			n1: Int, t: Double, delta: Double )
	: List[sysEntry] = {
			val elim = sysList.exists( i => i.eliminates(nextSys, n1, t, delta));
			var newList = sysList.filter( i => !(nextSys.eliminates(i, n1, t, delta)));
			if (!elim) newList = newList :+ nextSys;
			newList
	}

	def ListVsList( aList: List[sysEntry], bList: List[sysEntry],
			n1: Int, t: Double, delta: Double )
	: List[sysEntry] = {
			val aList_new = aList.filter{
				i => !(bList.exists( j => j.eliminates(i, n1, t, delta)))
			};
			val bList_new = bList.filter{
				j => !(aList.exists( i => i.eliminates(j, n1, t, delta)))
			};
			aList_new ::: bList_new
	}

	//	def ListVsBest( t: (Int, (List[sysEntry], Option[Iterable[sysEntry]])),
	//			n1: Int, rmax: Int, eta: Double)
	//			: List[sysEntry] = {
	//		if (t._2._1.isEmpty) Nil else {
	//			val aList = t._2._1;
	//			val bList = t._2._2;
	//			bList match {
	//			case Some( l ) =>
	//			aList.filter( i => !(l.exists( j => j.eliminates(i,n1,rmax,eta))))
	//			case None => aList
	//			}
	//		}
	//	}

	def main(args: Array[String]) = {
		val start_t = System.nanoTime();
		val outputFile = args(0);
		val n1 = args(1).toInt; // 50
		val batchSize = args(2).toInt; // 100
		val nCores = args(3).toInt;
		val nGroups = args(4).toInt;
		val globalSeed = args(5).toLong;
		//		val s1Max = args(6).toInt; // not used in NSGS
		val delta = args(7).toDouble;
		val param = args(8); // RB
		val cov = args(9);
		val runS2 = true;
		//		val runS3 = false && runS2; // not used in NSGS
		val RB = param.toInt;
		val numSys = TpMax.getNumSystems(param).toInt;
		val eta = EtaFunc.find_eta(n1, 0.025, numSys);
		val rinott_h = Rinott.rinott(numSys, 0.975, n1-1); 
		//		val rmax = if (s1Max > 0) (s1Max / batchSize) else -1; // not used in NSGS
		val r_tol = 500; // not used in NSGS
		println( f"Eta=$eta, nGroups=$nGroups, numSys=$numSys, h=$rinott_h");
		val conf = new SparkConf().setAppName("RnS-Spark-NSGSp").set("spark.cores.max", nCores.toString());
		val tDist = new TDistribution(n1-1);
		val t = tDist.inverseCumulativeProbability(Math.pow(0.975,(1.0/(numSys-1.0))));
		val f1 = toList[sysEntry](_);
		val f2 = (sysList: List[sysEntry], nextSys: sysEntry ) => ListVsEntry( sysList, nextSys, n1, t, delta );
		val f3 = (a: List[sysEntry], b: List[sysEntry]) => ListVsList( a, b, n1, t, delta );
		// Create a Scala Spark Context.
		val sc = new SparkContext(conf);
		// val groupsList = (0 to (nGroups - 1)).toList;
		// Initialize a list of systems
		val sysIDs = sc.parallelize((0 to (numSys - 1)).toList, nGroups * 5 );
		// Declare various accumulators for profiling
		val accum_S1_sim = sc.accumulator(0L, "Accumulator: Stage 1 Simulations");
		val accum_S2_sim = sc.accumulator(0L, "Accumulator: Stage 2 Simulations");
		val accum_S1_t = sc.accumulator(0L, "Accumulator: Stage 1 Run Time");
		val accum_S2_t = sc.accumulator(0L, "Accumulator: Stage 2 Run Time");
		val accum_Screen_t = sc.accumulator(0L, "Accumulator: Screen Time");

		// Stage 1: Simulate each system for n1 replications
		println("Entering Stage 1...");
		val stage1output = sysIDs.map(
				stage1mapper( _, n1, globalSeed, param, cov, accum_S1_sim, accum_S1_t )
				).cache();

		val s1Entries = stage1output.map{
			batchsizemapper( _, n1, nGroups, batchSize )
		};

    println("Entering Stage 1 screening...");
		// screening	
		var stage1ScreenOutput = s1Entries.combineByKey( f1, f2, f3 ).values.flatMap { x => x }.cache();
    val surviving = stage1ScreenOutput.count();
    
    println("Entering Stage 2...");
		var extraSims = stage1ScreenOutput.flatMap{
			calcS2Sample( _, delta, batchSize, rinott_h )
		}.repartition(nGroups * 5).map{
			runRinottSim(_, param, cov, accum_S2_sim, accum_S2_t, runS2 )
    };

		val allSims = stage1ScreenOutput.map{
			existingSample(_)
		}.union(extraSims).reduceByKey(
				(x, y) => {
					val sz = x._2 + y._2;
					( (x._1 * x._2 + y._1 * y._2) / sz, sz );
				}
				);
		val bestSys = allSims.reduce{
			(x, y) => if ( x._2._1 > y._2._1 ) x else y
		}._1;
		println("!Found Answer!");
		println(bestSys); 
		val final_t = (System.nanoTime() - start_t).toDouble/1e9;
		println(f"Total time = $final_t%.2f secs.");
    val sim_S1 = accum_S1_sim.value;
		val sim_S2 = accum_S2_sim.value;
		val sim_Total = sim_S1 + sim_S2;
    val t_S1 = accum_S1_t.value / 1e9;
    val t_S2 = accum_S2_t.value / 1e9;
		val t_Total = t_S1 + t_S2;
		val sim_util = t_Total / nCores / final_t;

		println("algo,n0,n1,bsize,bmax,delta,seed," +
				"screenVer,core,time,RB,cov,systems,phase2sys," +
				"simcount_0,simcount_1,simcount_2,simcount_3,totalsim," +
				"winner,simtime_0,simtime_1,simtime_2,simtime_3," +
				"simtime_total,simtime_util,screentime,screentime_util");
		println(f"SPK_NSGSp,0,$n1,$batchSize,0,$delta,$globalSeed," +
				f"8,$nCores,$final_t%.2f,$param,$cov,$numSys,$surviving," +
				f"0,$sim_S1,$sim_S2,0,$sim_Total," +
				f"$bestSys,0.0,$t_S1%.2f,$t_S2%.2f,0," +
				f"$t_Total%.2f,$sim_util,0,0");
	}
}
